from setuptools import setup, find_packages

setup(
    name='ziperator',
    version='1.0.0',
    description='Tool to convert ZIP and 7z archives of bin/cue or iso files to CHD format.',
    author='Your Name',
    author_email='your.email@example.com',
    py_modules=['ziperator'],  # This tells setuptools that there's a single module at the top level
    install_requires=[
        'py7zr',  # And any other dependencies
    ],
    entry_points={
        'console_scripts': [
            'ziperator=ziperator:main'
        ]
    },
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: End Users/Desktop',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',
        'Operating System :: OS Independent',
    ]
)
