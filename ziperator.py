import argparse
import rarfile
import zipfile
import sys
import subprocess
import os
import tempfile
import shutil
import py7zr
import syslog

def log(message, level=syslog.LOG_INFO):
    """
    Logs messages to both the system log and console.
    
    Args:
        message (str): The message to log.
        level (int): The logging level, e.g., syslog.LOG_INFO, syslog.LOG_ERR.
        
    Outputs to the console based on the severity of the log level and also sends the message to syslog.
    """
    # Mapping levels to console outputs explicitly
    if level == syslog.LOG_ERR:
        print(f"ERROR: {message}", file=sys.stderr)
    else:
        print(f"INFO: {message}", file=sys.stdout)
    
    syslog.syslog(level, message)


def run_external_tool_on_file(cue_file_path, temp_dir, output_dir):
    """
    Runs an external tool to create CHD files from cue files and moves the output.

    Args:
        cue_file_path (str): Path to the cue file.
        temp_dir (str): The temporary directory where operations are performed.
        output_dir (str): The destination directory to store the final CHD file.

    This function handles subprocess calls to 'chdman' and moves the generated CHD file to the output directory.
    """
    log(f"Attempting to process file: {cue_file_path}", syslog.LOG_INFO)
    try:
        name = os.path.splitext(os.path.basename(cue_file_path))[0]
        chd_file_path = os.path.join(temp_dir, f"{name}.chd")
        subprocess.run(['chdman', 'createcd', '-i', cue_file_path, '-o', chd_file_path, '--force'], check=True, cwd=temp_dir)
        shutil.move(chd_file_path, os.path.join(output_dir, os.path.basename(chd_file_path)))
        log(f"Successfully created and moved CHD file: {chd_file_path}")
    except subprocess.CalledProcessError as e:
        log(f"Error running external tool on file {cue_file_path}: {str(e)}", syslog.LOG_ERR)
    except shutil.Error as e:
        log(f"Error moving the CHD file: {str(e)}", syslog.LOG_ERR)


def decompress_and_process_rar_in_memory(rar_file_path, output_dir):
    """
    Decompresses RAR files in memory and processes contained cue files.
    
    Args:
        zip_file_path (str): The path to the zip file.
        output_dir (str): The directory to store the output CHD files.

    Extracts all files from the zip archive to a temporary directory,
    processes any .cue files found, and handles errors related to file integrity and access.
    """
    try:
        with rarfile.RarFile(rar_file_path, 'r') as rar_ref:
            with tempfile.TemporaryDirectory() as temp_dir:
                rar_ref.extractall(temp_dir)
                for root, dirs, files in os.walk(temp_dir):
                    for file in files:
                        if file.endswith('.cue'):
                            cue_file_path = os.path.join(root, file)
                            run_external_tool_on_file(cue_file_path, temp_dir, output_dir)
        log(f"Processed RAR file: {rar_file_path}")
    except rarfile.Error as e:
        log(f"Bad RAR file {rar_file_path}, it may be corrupted: {str(e)}", syslog.LOG_ERR)
    except FileNotFoundError:
        log(f"RAR file {rar_file_path} not found.", syslog.LOG_ERR)
    except PermissionError:
        log(f"Permission denied when accessing {rar_file_path}.", syslog.LOG_ERR)


def decompress_and_process_zip_in_memory(zip_file_path, output_dir):
    """
    Decompresses ZIP files in memory and processes contained cue files.
    
    Args:
        zip_file_path (str): The path to the zip file.
        output_dir (str): The directory to store the output CHD files.

    Extracts all files from the zip archive to a temporary directory,
    processes any .cue files found, and handles errors related to file integrity and access.
    """
    try:
        with zipfile.ZipFile(zip_file_path, 'r') as zip_ref:
            with tempfile.TemporaryDirectory() as temp_dir:
                zip_ref.extractall(temp_dir)
                for root, dirs, files in os.walk(temp_dir):
                    for file in files:
                        if file.endswith('.cue'):
                            cue_file_path = os.path.join(root, file)
                            run_external_tool_on_file(cue_file_path, temp_dir, output_dir)
    except zipfile.BadZipFile:
        log("Bad zip file {zip_file_path}, it may be corrupted.", syslog.LOG_ERR)
    except FileNotFoundError:
        log("Zip file {zip_file_path} not found.", syslog.LOG_ERR)
    except PermissionError:
        log("Permission denied when accessing {zip_file_path}.", syslog.LOG_ERR)


def decompress_and_process_7z_in_memory(sevenz_file_path, output_dir):
    """
    Decompresses 7z files in memory and processes contained cue files.
    
    Args:
        sevenz_file_path (str): The path to the zip file.
        output_dir (str): The directory to store the output CHD files.

    Extracts all files from the zip archive to a temporary directory,
    processes any .cue files found, and handles errors related to file integrity and access.
    """
    try:
        with py7zr.SevenZipFile(sevenz_file_path, 'r') as sevenz_ref:
            with tempfile.TemporaryDirectory() as temp_dir:
                sevenz_ref.extractall(temp_dir)
                for root, dirs, files in os.walk(temp_dir):
                    for file in files:
                        if file.endswith('.cue'):
                            cue_file_path = os.path.join(root, file)
                            run_external_tool_on_file(cue_file_path, temp_dir, output_dir)
    except py7zr.Bad7zFile:
        log("Bad 7z file {sevenz_file_path}, it may be corrupted.", syslog.LOG_ERR)
    except FileNotFoundError:
        log("7z file {sevenz_file_path} not found.", file=sys.stderr)
    except PermissionError:
        log("Permission denied when accessing {sevenz_file_path}.", syslog.LOG_ERR)


def main():
    log("Starting Ziperator", syslog.LOG_INFO)
    parser = argparse.ArgumentParser(description='Convert compressed .zip or .7z files containing bin/cue or iso files into compressed MAME CHD format.')
    parser.add_argument('-i', '--input', required=True, help='Input zip or 7z file path or directory containing such files.')
    parser.add_argument('-o', '--output', required=True, help='Output directory to store .chd files.')
    args = parser.parse_args()

    log(f"Input: {args.input}, Output: {args.output}", syslog.LOG_INFO)

    if not os.path.exists(args.input):
        log(f"Input path does not exist: {args.input}", syslog.LOG_ERR)
        sys.exit(1)
    if not os.path.exists(args.output):
        os.makedirs(args.output, exist_ok=True)
        log(f"Output directory created: {args.output}", syslog.LOG_INFO)

    if os.path.isdir(args.input):
        for item in os.listdir(args.input):
            full_path = os.path.join(args.input, item)
            print(f"Checking file: {full_path}")  # Debug print
            if os.path.isfile(full_path) and (full_path.lower().endswith('.zip') 
                    or full_path.lower().endswith('.7z') 
                        or full_path.lower().endswith('.rar')):
                if full_path.lower().endswith('.zip'):
                    decompress_and_process_zip_in_memory(full_path, args.output)
                elif full_path.lower().endswith('.7z'):
                    decompress_and_process_7z_in_memory(full_path, args.output)
                elif full_path.lower().endswith('.rar'):
                    decompress_and_process_rar_in_memory(full_path, args.output)


    elif os.path.isfile(args.input):
        if args.input.lower().endswith('.zip'):
            decompress_and_process_zip_in_memory(args.input, args.output)
        elif args.input.lower().endswith('.7z'):
            decompress_and_process_7z_in_memory(args.input, args.output)
        elif args.input.lower().endswith('.rar'):
            decompress_and_process_rar_in_memory(args.input, args.output)
    else:
        log("Invalid input path: {args.input}",  syslog.LOG_ERR)
        sys.exit(1)

    log("Ziperator finished processing", syslog.LOG_INFO)


if __name__ == "__main__":
    main()
