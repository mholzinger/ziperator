
# Ziperator

Ziperator is a command-line tool designed to convert ZIP and 7z archives containing bin/cue or iso files into the compressed MAME CHD format. This tool is intended to facilitate the conversion process with minimal disk space usage and efficient handling of compressed files.

## Requirements for rom-tools

This project relies on an open source project called `chdman` which is included in the project called `rom-tools`

This python wrapper assumed that the rom-tools packages including chdman have been installed are are callable through the $PATH variable.

### MacOS
In MacOS, `chdman` can be installed through [Homebrew](https://brew.sh/), with the following command:

```bash
brew install rom-tools
```


### Linux
On Debian based systems, including RetroPie, `chdman` can be found in the `mame-tools` package and can be installed with:

```bash
sudo apt install -y --no-install-recommends mame-tools
```

## Features

- Supports `.zip` and `.7z` files.
- Efficient in-memory processing for quick conversions.
- Easy to use command-line interface.

## Installation

To install Ziperator, follow these steps:

1. Clone this repository:
   ```bash
   git clone https://github.com/yourusername/ziperator.git
   ```
2. Navigate to the project directory:
   ```bash
   cd ziperator
   ```
3. Install the project dependencies:
   ```bash
   pip install -r requirements.txt
   ```
4. Install Ziperator:
   ```bash
   python setup.py install
   ```

## Usage

After installation, you can run Ziperator using the following command:

```bash
ziperator --input path/to/your/archive.zip --output path/to/output/directory
```

### Parameters:

- `-i`, `--input`: Specify the input ZIP or 7z file path or a directory containing such files.
- `-o`, `--output`: Specify the output directory to store the .chd files.

## Contributing

Contributions are welcome! Please feel free to submit a pull request or open an issue if you have feedback or would like to propose changes.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
